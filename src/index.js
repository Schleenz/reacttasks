import React from "react";
import ReactDOM from "react-dom"
import "./index.css";
import TaskList from "./tasklist.js";


let destination = document.querySelector("#taskplanner");

ReactDOM.render(
    <div>
        <TaskList/>
    </div>,
    destination
);